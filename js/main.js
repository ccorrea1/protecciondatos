const borrarClase = () => {
    let contenedorItem = document.getElementsByClassName('contenedorItem');
    for (let index = 0; index < contenedorItem.length; index++) {
        const element = contenedorItem[index];
        if (element.classList.contains('active')) {
            element.classList.remove('active')
        }
    }
}

const initItems = (id) => {
    let itemMenu = document.getElementsByClassName('itemMenu');
    let item = document.getElementById(id);
    for (let index = 0; index < itemMenu.length; index++) {
        const element = itemMenu[index];
        if (element.classList.contains('active')) {
            element.classList.remove('active')
        }
    }
    item.classList.add('active');
}

const itemsPoliticas = {
    politica: (id) => {
        initItems(id)
        let politicaPrivacidad = document.getElementById('politicaPrivacidad');
        borrarClase()
        politicaPrivacidad.classList.add('active');

    },
    solicitudes: (id) => {
        initItems(id)
        let solicitudesQuejasReclamos = document.getElementById('solicitudesQuejasReclamos');
        borrarClase()
        solicitudesQuejasReclamos.classList.add('active');
    },
    canales: (id) => {
        initItems(id)
        let canalesAtencion = document.getElementById('canalesAtencion');
        borrarClase()
        canalesAtencion.classList.add('active');
    },
    preguntas: (id) => {
        initItems(id)
        let preguntasFrecuentes = document.getElementById('preguntasFrecuentes');
        borrarClase()
        preguntasFrecuentes.classList.add('active');
    }
}

const showContent = (valor) => {
    let idItem = valor.id;
    itemsPoliticas[idItem](idItem)
    attachListener(valor, idItem)

}

const attachListener = (valor, idItem) => {
    if (valor.classList.contains('hasSubmenu')) {
        arrowToggleClass(valor)
        toggleSubMenu[idItem]()
    } else {
        removeClassToggleSubMenu()
    }
}


const arrowToggleClass = (valor) => {
    let papa = valor.childNodes;
    let hijo = papa[1].childNodes[5]
    hijo.classList.toggle('active')
}

const removeClassToggleSubMenu = () => {
    let subItem = document.getElementsByClassName('subItem');
    for (let index = 0; index < subItem.length; index++) {
        const element = subItem[index];
        element.classList.remove('active');
    }
}

const toggleSubMenu = {
    solicitudes: () => {
        let solicitudes = document.getElementById('solicitudesSubItem')
        solicitudes.classList.toggle('active');
    },
    preguntas: () => {
        let preguntas = document.getElementById('preguntasSubItem')
        preguntas.classList.toggle('active');
    }
}


const scrollIntoSection = (elemento) => {
    let dataID = elemento.getAttribute('data-id');
    setTimeout(() => { document.getElementById(dataID).scrollIntoView({ behavior: "smooth" }); }, 200);
}


const showContentMobile = (elemento) => {
    let idElement = elemento.id;
    leftActiveClass()
    elemento.classList.add('active')
    if(elemento.classList.contains('hasSubmenu')) {
        showContentMobileHidden[idElement]()
    } else {
        showHiddenContent[idElement](elemento)
        hiddenSubMenu()
    }
}



const hiddenSubMenu = () => {
    let menuControlesMobile = document.getElementById('menuControlesMobile');
    menuControlesMobile.classList.add('animate__animated', 'animate__fadeOutLeft')
    setTimeout(() => { 
        menuControlesMobile.classList.toggle('hiddenContent')
    }, 300);
}
// const showContentMobileHidden = (valor) => {
// let subItem = valor.childNodes[4]
// subItem.classList.toggle('active')
const showContentMobileHidden = {
    solicitudesMobile: () => {
        let solicitudes = document.getElementById('solicitudesSubMenuMobile')
        solicitudes.classList.toggle('active');
    },
    preguntasMobile: () => {
        let preguntas = document.getElementById('preguntasSubMenuMobile')
        preguntas.classList.toggle('active');
    }
}

// const showHiddenContent = (valor) => {
const showHiddenContent = {  
    politicaMobile: () => {
        let politicaMobileContent = document.getElementById('politicaMobileContent')
        politicaMobileContent.classList.toggle('active');
    },
    solicitudAccesoMobile: (elemento) => {
        let solicitudes = document.getElementById('solicitudesQuejasReclamosMobileContent')
        solicitudes.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    solicitudOposicionMobile: (elemento) => {
        let solicitudes = document.getElementById('solicitudesQuejasReclamosMobileContent')
        solicitudes.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    reclamosRectificacionMobile: (elemento) => {
        let solicitudes = document.getElementById('solicitudesQuejasReclamosMobileContent')
        solicitudes.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    reclamosCancelacionMobile: (elemento) => {
        let solicitudes = document.getElementById('solicitudesQuejasReclamosMobileContent')
        solicitudes.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    canalesMobile: () => {
        let canalesAtencionMobileContent = document.getElementById('canalesAtencionMobileContent')
        canalesAtencionMobileContent.classList.toggle('active');
    },
    informacionGeneralMobile: (elemento) => {
        let preguntasFrecuentesMobileContent = document.getElementById('preguntasFrecuentesMobileContent')
        preguntasFrecuentesMobileContent.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    ObjetivoDatosMobile: (elemento) => {
        let preguntasFrecuentesMobileContent = document.getElementById('preguntasFrecuentesMobileContent')
        preguntasFrecuentesMobileContent.classList.toggle('active');
        scrollIntoSection(elemento)
    },
    glosarioMobile: (elemento) => {
        let preguntasFrecuentesMobileContent = document.getElementById('preguntasFrecuentesMobileContent')
        preguntasFrecuentesMobileContent.classList.toggle('active');
        scrollIntoSection(elemento)
    }
}

const clickHiddenContent = () => {
    let contentHead = document.getElementsByClassName('contentHead')
    for (let index = 0; index < contentHead.length; index++) {
        const element = contentHead[index];
        element.onclick = hiddenMobileContent
    }
}

const leftActiveClass = () => {
    itemMenu = document.getElementsByClassName('itemMenu');
    for (let index = 0; index < itemMenu.length; index++) {
        const element = itemMenu[index];
        if(element.classList.contains('active')) {
            element.classList.remove('active')
        }
    }
}

const hiddenMobileContent = () => {
    leftActiveClass()
    let menuControlesMobile = document.getElementById('menuControlesMobile');
    setTimeout(()=> {
        menuControlesMobile.classList.remove('animate__fadeOutLeft')
        menuControlesMobile.classList.add('animate__fadeInLeft')
        menuControlesMobile.classList.remove('hiddenContent')
    }, 200)
    // menuControlesMobile.classList.add('animate__animated')
    let hiddenContent = document.getElementsByClassName('hiddenContent')
    for (let index = 0; index < hiddenContent.length; index++) {
        const element = hiddenContent[index];
        element.classList.remove('active')
        // element.classList.remove('animate__fadeinRight')
        // element.classList.add('animate__fadeOutRight')
    }
}

const functionsInitDOM = () => {
    //write your functions initial dom
    clickHiddenContent()
}



document.addEventListener("DOMContentLoaded", functionsInitDOM, false);